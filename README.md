# ww-box
Box integration for Watson Workspace

This is quite basic. You can add the tampermonkey script to Chrome, and use it to upload a file to the shared Sametime space. You can then, if you are in the Sametime support space, search for documents stored there.

That's all. No point in getting too fancy since official integration is coming soon.

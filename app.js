const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const fs = require("fs");
const https = require("https");
const formidable = require("formidable");
const util = require("util");
const winston = require("winston");
const ww = require("./lib/ww");
const box = require("./lib/box");
const boxbot = require("./boxbot.js");

const SPACE_ID = "575970fdb684e23f934656fe";
const WW_URL = "https://api.watsonwork.ibm.com";

var APP_ID = process.env.APP_ID;
var APP_SECRET = process.env.APP_SECRET;
var WEBHOOK_SECRET = process.env.WEBHOOK_SECRET;
var PORT = process.env.PORT;
var privateKey = fs.readFileSync("key.pem");
var certificate = fs.readFileSync("cert.pem");
var app = express();
var token = {};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {'timestamp':true});
winston.level = "debug";


// Accept file uploads from a space.
app.post("/upload", function(req, res) {
  winston.log("info", "Received POST to /upload");
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    boxbot.handleUpload(fields, files, function (err, uploaded) {
      if (err) {
        winston.log("error", "Could not properly handle upload from space.");
        winston.log("error", err);
        res.writeHead(500, {"content-type": "text/plain"});
        res.write("Could not upload file: " + err);
        res.end();
      } else {
        res.writeHead(200, {"content-type": "text/plain"});
        res.write("Received upload:\n\n");
        res.end(util.inspect({fields: fields, files: files}));
      }
    });
  });
});

// Callback from Watson Workspace.
app.post("/ww", function (req, res) {
  winston.log("info", "Received POST to /ww");
  var body = req.body;
  var eventType = body.type;

  // Verification event
  if (eventType === "verification") {
    winston.log("debug", "Verifying...");
    ww.verifyWorkspace(res, body.challenge, WEBHOOK_SECRET);
    return;
  }
  res.status(200).end();
  // Message created event
  if (eventType === "message-created") {
    // Ignore our own messages
    if (body.userId === APP_ID) {
      return;
    }
    var text = body.content.toLowerCase();
    // Handle if we were mentioned
    if (text.includes('@box')) {
      winston.log("info", "We were mentioned in a message");
      winston.log("debug", "Message was: " + text);
      boxbot.handleMessage(body, token, SPACE_ID);
    }
  }
});

https.createServer({
  key: privateKey,
  cert: certificate
}, app).listen(PORT, function (err, res) {
  winston.log("info", "Server started on port " + PORT);
});

// Grab our initial WW auth token. They're good for roughly 12 hours.
ww.getToken(WW_URL, APP_ID, APP_SECRET, function (err, res) {
  if (err) {
    winston.log("error", "Failed to obtain initial token", err);
  } else {
    token = res;
  }
});

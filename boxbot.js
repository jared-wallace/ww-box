const request = require("request");
const fs = require("fs");
const box = require("./lib/box");
const ww = require("./lib/ww");
const winston = require("winston");

module.exports.handleMessage = function (data, token, space) {
  winston.log("debug", "Entered boxbot.handleMessage");
  var msg_id = data.messageId;

  var body = "{ message (id: \"" + msg_id + "\")" +
    "{ createdBy { displayName id }" +
    " created content contentType annotations } }";
  ww.makeGraphQLCall(body, token, function (err, res) {
    if (err) {
      winston.log("error", "MakeGraphQL call failed", err);
      return;
    }
    winston.log("debug", "GraphQL returned: ", res);

    // We don't do anything with the metadata we get back yet, but we have
    // an easy way to do more later if we wish.
    var data = res.data.message;
    var WKeywords = [];
    var annotation_length = data.annotations.length;
    var msg = "";

    for (var i = 0; i < annotation_length; i++) {
      try {
        var note = JSON.parse(data.annotations[i]);
        if (note.type === "message-nlp-keywords") {
          WKeywords = note.keywords;
        }
      }
      catch (e) {
        winston.log("error", "Failed to parse annotation #" + i);
      }
    }
    winston.log("debug", "Alchemy keywords are: " + WKeywords.toString());
    // The message text is UTF-8, so we need to first decode it to be safe.
    var message = decode_utf8(data.content);

    // What are we being asked to do?
    if (message.includes("search")) {
      searchBox(message, token, space);
    } else if (message.includes("details")) {
      getDetails(message, token, space);
    } else {
      ww.sendMessage("Sorry, I didn't understand", "#016F4A", space, token);
    }
  });
}

module.exports.handleUpload = function (fields, files, callback) {
  winston.log("debug", "Entered boxbot.handleUpload");
  winston.log("debug", "files obj: ", files);
  winston.log("debug", "fields obj: ", fields);
  var name = files.file.name;
  var path = files.file.path;
  var size = files.file.size;
  var parent_folder = fields.parent_id;
  var client = box.getUserClient();
  // Check for possible issues
  fileExists(name, client, size, parent_folder, function (err, res) {
    if (res) {
      updateFile(res, path, client, function (err, res) {
        callback(err, res);
      });
    } else {
      uploadFile(name, path, size, parent_folder, client, function (err, res) {
        callback(err, res);
      });
    }
  });
}

var updateFile = function (id, path, client, callback) {
  winston.log("debug", "Entered boxbot.updateFile");
  var stream = fs.createReadStream(path);
  client.files.uploadNewFileVersion(id, stream, function (err, res) {
    if (err) {
      winston.log("error", "Could not update file in Box", err);
      callback(err, null);
    } else {
      winston.log("info", "File was successfully updated in Box");
      callback(null, true);
    }
    deleteLocalCopy(path);
  });
}

var uploadFile = function (name, path, size, parent_folder, client, callback) {
  winston.log("debug", "Entering boxbot.uploadFile");
  var stream = fs.createReadStream(path);
  client.files.uploadFile(parent_folder, name, stream, function (err, res) {
    if (err) {
      winston.log("error", "Could not upload file to Box", err);
      callback(err, null);
    } else {
      winston.log("info", "File was successfully uploaded to Box");
      winston.log("debug", "Creating shared link");
      var id = res.entries[0].id;
      client.files.update(id, {shared_link: client.accessLevels.DEFAULT}, function (err, created) {
        if (err) {
          winston.log("error", "Failed to create shared link for file.", err);
          callback(err, null);
        } else {
          winston.log("debug", "Created shared link for file.")
          callback(null, true);
        }
      });
    }
    deleteLocalCopy(path);
  });
}

var deleteLocalCopy = function (path) {
  winston.log("debug", "Entered boxbot.deleteLocalCopy");
  fs.unlink(path, function (err, res) {
    if (err) {
      winston.log("error", "Could not delete file " + path, err);
    } else {
      winston.log("debug", "File " + path + " was deleted successfully");
    }
  });
}

var fileExists = function (name, client, size, parent_folder, callback) {
  winston.log("debug", "Entered boxbot.fileExists");
  client.files.preflightUploadFile(parent_folder, {name: name, size: size}, null, function (err, res) {
    if (err) {
      winston.log("debug", "Preflight failed", err);
      callback(null, err.response.body.context_info.conflicts.id);
    } else {
      winston.log("debug", "Preflight succeeded");
      callback(null, false);
    }
  });
}

var searchBox = function (msg, token, space) {
  winston.log("debug", "Entered boxbot.searchBox");
  var query = msg.replace("@box search", "");
  winston.log("debug", "Query is: " + query);
  var client = box.getUserClient();
  client.search.query(query, {
    fields: 'name,tags,description,version_number,shared_link,modified_at',
    limit: 5,
    offset: 0
    }, function (err, res) {
    if (err) {
      winston.log("error", "Search failed", err);
    } else {
      if (res.total_count < 1) {
        msg = "Sorry, no files matching that description could be found.";
      } else {
        winston.log("debug", "Box search result:", res);
        msg = "I found the following results:\n";
        for (var x = 0; x < res.total_count; x++) {
          var file = res.entries[x];
          winston.log("debug", "File entry: ", file);
          try {
            msg += "[" + file.name + "](" + file.shared_link.download_url + ")";
            if (file.version_number > 1) {
              msg += " _version " + file.version_number + "_";
              var timestamp = Date.parse(file.modified_at);
              var time = new Date(timestamp);
              //msg += " last modified at " + time.toString() + "_\n"
            }
            msg += "\n\t_" + file.description + "_\n";
          } catch (err) {
            winston.log("error", "unexpected file structure");
          }
        }
      }
      ww.sendMessage(msg.slice(0,-1), "#016F4A", space, token);
    }
  });
};

var getDetails = function (msg, token, space) {
  winston.log("debug", "Entered boxbot.getDetails");
};

var decode_utf8 = function (s) {
  winston.log("debug", "Entered talkback.decode_utf8.");
  return unescape(encodeURIComponent(s));
}

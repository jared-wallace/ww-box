// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://workspace.ibm.com/*
// @grant GM_xmlhttpRequest
// ==/UserScript==

// Declare constants
const UPLOAD_URL = "https://www.jared-wallace.com:4444/upload";

// Create and odify the new elements
var newDiv = document.createElement('div');
var newButton = document.createElement("input");
var boxInput = document.createElement("input");

boxInput.setAttribute("id", "fileInput");
boxInput.type = "file";
boxInput.style = "display:none";

newButton.type = "button";
newButton.value = "box";

newDiv.classList.add("ic-composer-action");
newButton.classList.add("ic-composer-action-button");

newButton.appendChild(boxInput);
newDiv.appendChild(newButton);

// When they click the button, "click" the hidden form.
newButton.addEventListener("click", function() {
  document.getElementById("fileInput").click();
});

// Declare handler for after file is selected.
boxInput.addEventListener("change", handleFiles, false);

function handleFiles() {
  if (!this.files) {
      return;
  }
  var fileList = this.files;
  var data = new FormData();
  data.append("file", fileList[0]);
  data.append("parent_id", "0");
  GM_xmlhttpRequest({
    method: "POST",
    url: UPLOAD_URL,
    data: data,
    headers: {
        "ContentType": "multipart/form-data"
    },
    onload: function(response) {
      console.log(response.responseText);
      alert(fileList[0].name + " successfully uploaded.");
    }
  });
}


setTimeout(function() {
  var firstButton = document.getElementsByClassName("ic-composer-action")[1];
  firstButton.parentNode.insertBefore(newDiv, firstButton);
}, 2000);

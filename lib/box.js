const request = require("request");
const fs = require("fs");
const path = require("path");
const ww = require("./ww");
const boxSDK = require("box-node-sdk");
const winston = require("winston");

const BOX_OAUTH_URL = "https://account.box.com/api/oauth2/";
const BOX_CLIENT_ID = "lrkhlxx0430ttczbhrvv3h88emkcp74r";
const BOX_CLIENT_SECRET = "cqLHq2SED1UdxHT08FVLs5LTv7bDnl2W";
const BOX_ENTERPRISE_ID = "17555949";
const BOX_PRIVKEY_PASSPHRASE = process.env.BOX_PRIVKEY_PASSPHRASE;
const BOX_PUBKEY_ID = process.env.BOX_PUBKEY_ID;
const BOX_USER_ID = "1528857750";

var BOX_PRIVKEY = "";
var sdk = {};

// Setup our box sdk
winston.log("debug", "Path to private key is: " + path.join(__dirname, "private_key.pem"));
fs.readFile(path.join(__dirname, "private_key.pem"), "utf8", function (err, res) {
  if (err) {
    winston.log("error", "Could not read private key file.", err);
  } else {
    BOX_PRIVKEY = res;
    sdk = new boxSDK({
      clientID: BOX_CLIENT_ID,
      clientSecret: BOX_CLIENT_SECRET,
      appAuth: {
        keyID: BOX_PUBKEY_ID,
        privateKey: BOX_PRIVKEY,
        passphrase: BOX_PRIVKEY_PASSPHRASE,
      }
    });
    winston.log("debug", "Box SDK initialized");
  }
});


module.exports.getEnterpriseClient = function () {
  winston.log("debug", "Entered lib.box.getEnterpriseClient");
  var client = sdk.getAppAuthClient("enterprise", BOX_ENTERPRISE_ID);
  return client;
}

module.exports.getUserClient = function (id) {
  winston.log("debug", "Entered lib.box.getuserClient");
  if (!id) {
    winston.log("info", "Using default user id for Box");
    id = BOX_USER_ID;
  }
  var client = sdk.getAppAuthClient("user", id);
  return client;
}

module.exports.createUser = function (name, callback) {
  winston.log("debug", "Entered lib.box.createUser");
  var client = module.exports.getEnterpriseClient();
  client.post('/users', {body: {name: name, "is_platform_access_only": true}}, function(err, response) {
    if (!err) {
      // Full response looks like:
      // { type: 'user',
      //   id: '1528857750',
      //   name: 'App User Name',
      //   login: 'AppUser_356142_3CQbyK0Gq4@boxdevedition.com',
      //   created_at: '2017-04-22T13:52:59-07:00',
      //   modified_at: '2017-04-22T13:52:59-07:00',
      //   language: 'en',
      //   timezone: 'America/Los_Angeles',
      //   space_amount: 10737418240,
      //   space_used: 0,
      //   max_upload_size: 2147483648,
      //   status: 'active',
      //   job_title: '',
      //   phone: '',
      //   address: '',
      //   avatar_url: 'https://jared-wallace.app.box.com/api/avatar/large/1528857750'
      // }
      winston.log("info", "Created new user " + name + " with id: " + response.body.id);
      callback(null, true);
    } else {
      winston.log("error", "Could not create new user", err);
      callback(err, null);
    }
  });
}
